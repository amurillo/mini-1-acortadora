import webapp
from urllib.parse import unquote
import string
import random
import http.cookies
import shelve

form = """
<form action="" method="POST">
    <p>URL: <input type="text" name="url"></p>
    <p><input type="submit" value="Submit"></p>
</form>
"""


page = """
<html>
    <body>
        <h1>Stored URLs for browser {id_cookie}</h1>
            <table class='default'>{table}</table>
        <h1>Formulary:</h1>
            <p>{form}</p>
    </body>
</html>
"""

resource_not_found = """
<html>
    <body>
        <h1>Resource not found</h1>
    </body>
</html>
"""


class RandomShortApp(webapp.webApp):
    def __init__(self, hostname, port):
        self.browsers = shelve.open('browsers', flag='c', writeback=True)
        super().__init__(hostname, port)

    @staticmethod
    def find_cookie(headers):
        found = False
        pos = 0
        id_cookie = ''
        while not found and pos < len(headers):
            if headers[pos].startswith('Cookie: '):
                cookies = headers[pos].split(': ', 2)[1]
                cookies_list = cookies.split('; ')

                for cookie in cookies_list:
                    if cookie.startswith('id'):
                        id_cookie = str(cookie.split('=')[1])
                        break

                found = True

            else:
                pos += 1

        return id_cookie

    def parse(self, request):
        headers = (request.split('\r\n\r\n', 1)[0]).split('\r\n')[1:]
        data = {'method': request.split(' ', 2)[0],
                'resource': request.split(' ', 2)[1],
                'id_cookie': self.find_cookie(headers),
                'body': unquote(request.split('\r\n\r\n', 1)[1])}

        return data

    def process(self, data):
        if data['method'] == 'GET':
            http_code, html_body = self.get(data['resource'], data['id_cookie'])
        elif data['method'] == 'POST':
            http_code, html_body = self.post(data['body'], data['id_cookie'])

        return http_code, html_body

    def stored_urls(self, id_cookie):
        table = """
        <tr>
            <th>Resource</th>
            <th>URL</th>
        </tr>
        """
        for resource, url in self.browsers[id_cookie].items():
            table += f"""
            <tr>
                <td>{resource}</td>
                <td>{url}</td>
            </tr>
            """

        return table

    def show_urls(self, id_cookie):
        http_code = '200 OK \r\n'

        if (id_cookie == '') or (id_cookie not in self.browsers.keys()):
            cookies = http.cookies.SimpleCookie()
            id_cookie = ''.join(random.choices(string.ascii_lowercase + string.digits, k=10))
            cookies['id'] = id_cookie
            self.browsers[id_cookie] = {}
            http_code += cookies.output() + '\r\n'
            html_body = page.format(id_cookie=id_cookie, table=self.stored_urls(id_cookie), form=form)

        else:
            html_body = page.format(id_cookie=id_cookie, table=self.stored_urls(id_cookie), form=form)

        return http_code, html_body

    def get(self, resource, id_cookie):
        if resource == '/':
            http_code, html_body = self.show_urls(id_cookie)

        elif resource in self.browsers[id_cookie]:
            html_body = ''
            http_code = '301 Moved Permanently\r\n' + 'Location: ' + self.browsers[id_cookie][resource] + '\r\n\r\n'

        else:
            html_body = resource_not_found
            http_code = '404 Not Found'

        return http_code, html_body

    def post(self, body, id_cookie):
        url = unquote(body.split("=")[1])
        if not url.startswith('http') or not url.startswith('https'):
            url = 'https://' + url

        if url not in self.browsers[id_cookie].values():
            shortened = ''.join(random.choices(string.ascii_lowercase + string.digits, k=5))
            resource = '/' + shortened
            self.browsers[id_cookie][resource] = url

        http_code, html_body = self.show_urls(id_cookie)

        return http_code, html_body


if __name__ == "__main__":
    try:
        testRandomShortApp = RandomShortApp("", 1234)
    except KeyboardInterrupt:
        testRandomShortApp.browsers.close()
